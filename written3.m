clc
clear 
close all
%%
I=imread('images/Moire.jpg');
figure; imshow(uint8(I),[])
% for sigma1=0.01:0.03:0.1

sigma1=10
T=I;
for i=1:3
    T=imgaussfilt(T,sigma1/(2^i));
    T=imresize(T,.5);
   
end


H(1)=figure; imshow(uint8(T),'InitialMagnification','fit')
saveas(H(1),'output_images/written3_a.jpg')
% end

%%
sigma2=2                
I2=imread('images/Seattle.jpg');
figure; imshow(uint8(I2),[])

% for sigma2=0.1:0.3:3.0
T2=I2;
for i=1:3
    T2=imgaussfilt(T2,sigma2/(2^i));
    T2=imresize(T2,.5);
%     
    
end
T2=imgaussfilt(T2,sigma2);
H(2)=figure; imshow(uint8(T2),'InitialMagnification','fit')
saveas(H(2),'output_images/written3_b.jpg')
% end