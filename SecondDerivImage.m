function [ output_image ] = SecondDerivImage( I,sigma )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here


k=1; 
kp=2*k+1;  % size of filter

[M,N,P]=size(I);

I_pad=zeros(M+2*k,N,P);
I_pad(k+1:M+k,:,:)=I(1:M,:,:);

I_derx1=diff(I_pad,2);

I_padx=zeros(M+2*k,N,P);

I_padx(k+1:M+k,:,:)=I_derx1(1:M,:,:); %first differential in x

I_derx2=diff(I_padx,2); %second differential in x

I_dery1=diff(I_pad); %first differential in y
I_dery2=diff(I_dery1); %second differential in y

I_der=I_derx2+I_dery2; %summation second differentials

I_SD=GuassianBlurImage(I_der,sigma); % gaussian blurring
output_image=I_SD+128;


end

