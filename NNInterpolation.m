function [ I_s ] = NNInterpolation( I ,x,y)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
[M,N,P]=size(I);
if x<M
m=round(x,0);
else 
    m=M;
end

if y<N
n=round(y,0); %finding the nearest corner point
else
    n=N;
end
    
I_s=I(m,n);
 
end

