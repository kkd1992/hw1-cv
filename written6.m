clc
clear
close all

%%
I=imread('images/Gogh.png');
figure;imshow(I);
sigma=25;
% for sigma=1:2:10
I1=imgaussfilt(I,sigma);
%%
% for thres=10:10:100
thres=30
I2=FindPeaksImage(I1,thres);
%%
H(1)=figure;imshow(uint8(I1),[]);
H(2)=figure;imshow(uint8(I2),[]);
saveas(H(1),'output_images/written6a.png')
saveas(H(2),'output_images/GoghEdge.png');
% end

%%