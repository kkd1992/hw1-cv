%% Computer Vision Homework assignment 1
% Krishna Dontaraju - VU90819
clc
clear
close all

% Instructions to run the code
% input images should be in the folder 'images'
% output folders will created in folder 'output/images'
% run the main file for calling all functions 
 %% Question 1 Guassian filter

sigma=4;
fprintf('1. Gaussian filter with sigma = %d\n',sigma);
fprintf('\tInput Image - Seattle.jpg\n');
in1_I=imread('images/Seattle.jpg');
t_start1=tic; %start timer
out1_I=GuassianBlurImage(in1_I,sigma); 
time1=toc(t_start1);
fprintf('\tFiltering time = %2.2f \n',time1);
figure;title('Gaussian filter input');imshow(in1_I);
H(1)=figure;imshow(uint8(out1_I),[]);
title('Gaussian filter output');
saveas(H(1),'output_images/1.png');

%% Question 2 SeparableGaussianBlur
sigma=4;
fprintf('2. Seperable Gaussian filter with sigma = %d\n',sigma);
fprintf('\tInput Image - Seattle.jpg\n');
in2_I=imread('images/Seattle.jpg');        
t_start2=tic;
out2_I=SeparableGaussianBlurImage(in2_I,sigma);
time2=toc(t_start2);
fprintf('\tFiltering time = %2.2f \n',time2);
figure;imshow(in2_I);title('Seperable Gaussian filter input');
H(2)=figure;imshow(uint8(out2_I),[]);title('Seperable Gaussian filter output');
saveas(H(2),'output_images/2.png');

%% Question 3 Derivatives
sigma=1;
fprintf('3. Derivatives filter with sigma = %d\n',sigma);
fprintf('\tInput Image - LadyBug.jpg\n');
in3_I=imread('images/LadyBug.jpg');
out3a_I=FirstDerivImage(in3_I,sigma);
out3b_I=SecondDerivImage(in3_I,sigma);
figure;title('Derivatives filter input');imshow(in3_I);
H(3)=figure;imshow(uint8(out3a_I),[]);title('First Derivative filter output');
saveas(H(3),'output_images/3a.png');
H(4)=figure;imshow(uint8(out3b_I),[]);title('Second Derivative filter output');
saveas(H(4),'output_images/3b.png');

%% Question 4 Sharpen Image

sigma=1; alpha=5;
fprintf('4. Sharpen filter with sigma = %d, alpha =%d\n',sigma,alpha);
fprintf('\tInput Image - Yosemite.png\n');
in4_I=imread('images/Yosemite.png');
figure;title('sharpen Image input');imshow(in4_I); 
out4_I=SharpenImage(in4_I,sigma,alpha);

H(5)=figure;imshow(uint8(out4_I),[]);title('sharpen Image output');
saveas(H(5),'output_images/4.png');

%% Question 5 Sobel Image

sigma=4;
fprintf('5. Sobel edge filter with sigma = %d\n',sigma);
fprintf('\tInput Image - LadyBug.jpg\n');
in5_I=imread('images/LadyBug.jpg');
figure;title('Sobel filter input');imshow(in5_I);
[out5_mag,out5_dir]=SobelImage(in5_I);

H(6)=figure;imshow(out5_mag,[]);title('Sobel filter magnitude output');
saveas(H(6),'output_images/5a.png');
H(7)=figure;imshow(out5_dir,[]);title('Sobel filter direction output');
saveas(H(7),'output_images/5b.png');

%% Question 6 Bilinear Interpolation

in6_I=imread('images/Moire_small.jpg');
figure;title('Bilinear Interpolation input');imshow(in6_I);
in6_I=rgb2gray(in6_I);  
[M,N,P]=size(in6_I);
s=4;%scale

fprintf('6. Bilinear Interpolation with scale =%d\n',s);
fprintf('\tInput Image - Moire_small.jpg\n');

I_s=zeros(s*M,s*N)*255; 
I_s(1:s:s*M,1:s:s*N)=in6_I(1:M,1:N);
for m=1:s*M-1
    for n=1:s*N-1
        [out6a_I(m,n)]=BilinearInterpolation(in6_I,1+m/s,1+n/s); 
    end
end
% out6a_I=I_s;
H(8)=figure; title('Bilinear Interpolation output');imshow(out6a_I);
saveas(H(8),'output_images/6a.png');

for m=1:s*M-1
    for n=1:s*N-1
        [out6b_I(m,n)]=NNInterpolation(in6_I,1+m/s,1+n/s); 
    end
end
H(9)=figure;imshow(out6b_I); title('Nearest neighbor output');
saveas(H(9),'output_images/6b.png');

%% Question 7 finding peaks

thres=40;
fprintf('7. Finding peaks with threshold =%d\n',thres);
fprintf('\tInput Image - Circle.png\n');

in7_I=imread('images/Circle.png');
figure;title('finding peaks input');imshow(in7_I);
[out7_I]=FindPeaksImage(in7_I,thres);

H(10)=figure;imshow(out7_I);title('finding peaks output');
saveas(H(10),'output_images/7.png');

%% Question 8 Bilateral Image

SigmaS=4;
SigmaI=20;
fprintf('8. Bilateral Filter with SigmaS= %d, SigmaI= %d \n',SigmaS,SigmaI);
fprintf('\tInput Image - Seattle.jpg\n');
in8_I=imread('images/Seattle.jpg');
t_start8=tic;
out8_I=BilateralImage(in8_I,SigmaS,SigmaI);
time8=toc(t_start8);  
figure;imshow(in8_I);title('Bilateral image input');
H(11)=figure;imshow(uint8(out8_I),[]);title('Bilateral image output');
saveas(H(11),'output_images/8.png');


%% Question 9 Hough Transform
fprintf('9.Hough Transform \n');
fprintf('\tInput Image - gantrycrane.png\n');
thres=40;s=10;%s minimum degree to search for line(imcrease for faster implentation)
in9_I=imread('gantrycrane.png');
[out9_values,out9_weights]=HoughTransform(in9_I,thres,s);

        
[M,N,P]=size(out9_values);
s=pi/N;
% hough_lines=out9_values(max(out9_weights));
H(12)=figure;imshow(in9_I);hold on;
for n=1:N
   hough_lines(n,1)=out9_values(max(out9_weights(:,n)));
            
                plot_H_lines(hough_lines(n,1),n*s,size(in9_I,2));
           
end
saveas(H(12),'output_images/9.jpg');




  
