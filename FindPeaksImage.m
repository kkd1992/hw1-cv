function [ peak_response ] = FindPeaksImage( I,thres)
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here
[peak_mag,peak_dir]=SobelImage(I); %applying sobel
[M,N,P]=size(I);
peak_response=zeros(M,N);
peak_response(peak_mag>thres)=peak_mag(peak_mag>thres); %finding values greater than threshold 
e0=zeros(M,N);
e1=zeros(M,N);
for m=1:M
    for n=1:N
        if(peak_mag(m,n)>thres)               
            % the corrdinates perpendicular to the gradient line will be
            %(x+cos(theta-90)),(y+sin(theta-90))
            %(x-cos(theta-90)),(y-sin(theta-90))
            %where theta is angle of the gradient
            %x,y is the gradient location
            %interpolating for this new points using bilinear interpolation
        e0(m,n)=BilinearInterpolation(peak_mag,m+cos(peak_dir(m,n)-pi/2),n+sin(peak_dir(m,n)-pi/2)); 
        e1(m,n)=BilinearInterpolation(peak_mag,m-cos(peak_dir(m,n)-pi/2),n-sin(peak_dir(m,n)-pi/2));
       
        end
    end
end

peak_response(peak_mag>e0 & peak_mag>e1)=255;

end

