
clc
clear all
close all
%%
I=imread('images/Seattle.jpg');
figure;imshow(I);
sigmaS=4;
sigmaI=20;
% for sigma=1:2:10
I1=BilateralImage(I,sigmaS,sigmaI);
%%
% for thres=10:10:100
thres=50
I2=FindPeaksImage(I1,thres);
%%
H(1)=figure;imshow(uint8(I1),[]);
H(2)=figure;imshow(uint8(I2),[]);
saveas(H(1),'output_images/written7a.png')
saveas(H(2),'output_images/written7b.png');
% end