function [ output_image ] = FirstDerivImage( I,sigma )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here




k=1; 
kp=2*k+1;  % size of filter



[M,N,P]=size(I);
I_pad=zeros(M,N+2*k,P);

%zero padding
I_pad(:,k+1:N+k,:)=I(:,1:N,:);
[Mp,Np,P]=size(I_pad);

I_derx=diff(I_pad,2); %differential
I_FD=GuassianBlurImage(I_derx,sigma); %gaussian
output_image=I_FD+128;

end

