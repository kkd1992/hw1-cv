function [output_image ] = GuassianBlurImage( I, sigma )
%The function blurs the image using guassian filter
%   I=input Image
%   sigma = standard deviation

k=1; 
kp=2*k+1;  % size of filter
X=zeros(kp,kp); %filter matrix initialization
[Y,X]=meshgrid(-k:k,-k:k); %index of grids fpr calculating gaussian

G_2Dkernal=(1/2*pi*sigma^2)*exp(-(X.^2+Y.^2)/(2*sigma^2)); %2D kernal for gaussian
G_2Dkernal=G_2Dkernal/norm(G_2Dkernal);%normalizing

 G_3Dkernal=zeros(kp,kp,kp);
 for i=1:3
 G_3Dkernal(:,:,i)=G_2Dkernal; %copying the 2d kernal three times into 3 dimensions
end

%padding Image
[M,N,P]=size(I);
I_pad=zeros(M+2*k,N+2*k,P);
I_pad(k+1:M+k,k+1:N+k,:)=I(1:M,1:N,:);

[Mp,Np,P]=size(I_pad);

for m = 1:Mp-kp+1
    for n= 1:Np-kp+1
        
        if P==3
       I_pad(m+k,n+k,:)=(1/9)*sum(sum(G_3Dkernal.*I_pad(m:m+kp-1,n:n+kp-1,:))); %convolution for 3D image
        else
       I_pad(m+k,n+k,:)= (1/9)*sum(sum(G_2Dkernal.*I_pad(m:m+kp-1,n:n+kp-1)));%convolution for 2D image
        
        end

        
    end

end
%m:m+kp-1,n:n+kp-1,2
output_image=I_pad(k+1:M+k,k+1:N+k,:)*4;

