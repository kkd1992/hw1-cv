sigma=4;
close all;
I=imread('images/LadyBug.jpg');
% out3a_I=FirstDerivImage(I,sigma);
%out3b_I=SecondDerivImage(in3_I,sigma);
% figure;imshow(in3_I);
% H(3)=figure;imshow(out3a_I);
% saveas(H(3),'output_images/3a.png');
%H(4)=figure;imshow(out3b_I);
%saveas(H(4),'output_images/3b.png');


k=1; 
kp=2*k+1;  % size of filter
X=zeros(kp,kp); %filter matrix initialization

% Dx_kernal(:,:,1)=[-1 0 1];
% Dx_kernal(:,:,2)=[-1 0 1];
% Dx_kernal(:,:,3)=[-1 0 1];
[M,N,P]=size(I);
I_pad=zeros(M+2*k,N+2*k,P);

I_pad(k+1:M+k,k+1:N+k,:)=I(1:M,1:N,:);
[Mp,Np,P]=size(I_pad);

sigma=1;
I_derx=diff(I_pad,2);
I_FD=GuassianBlurImage(I_derx,sigma);
output_image=I_pad(k+1:M+k,k+1:N+k,:)+128;
 H(3)=figure;imshow(output_image);
% saveas(H(3),'output_images/3a.png');