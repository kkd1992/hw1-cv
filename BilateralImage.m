function [ output_image ] = BilateralImage( I,SigmaS,SigmaI )
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here


k=1; 
kp=2*k+1;  % size of filter
X=zeros(kp,kp); %filter matrix initialization
[Y,X]=meshgrid(-k:k,-k:k);

Gs_2Dkernal=(1/2*pi*SigmaS^2)*exp(-(X.^2+Y.^2)/(2*SigmaS^2)); %2d Gs kernal
Gs_2Dkernal=Gs_2Dkernal/norm(Gs_2Dkernal);

 Gs_3Dkernal=zeros(kp,kp,kp);
 for i=1:3
 Gs_3Dkernal(:,:,i)=Gs_2Dkernal;
 end

%padding
[M,N,P]=size(I);
I_pad=zeros(M+2*k,N+2*k,P);
I_pad(k+1:M+k,k+1:N+k,:)=I(1:M,1:N,:);


[Mp,Np,P]=size(I_pad);

for m = 1:Mp-kp+1
    for n= 1:Np-kp+1   
        Gr_3Dkernal=(1/2*pi*SigmaI^2)*exp(-(((I_pad(m:m+kp-1,n:n+kp-1,:)-I_pad(m+k,n+k,:)).^2)/(2*SigmaI^2))); %calculating intensity gaussian kernal
        Gr_3Dkernal=Gr_3Dkernal./(cat(3,norm(Gr_3Dkernal(:,:,1)),norm(Gr_3Dkernal(:,:,2)),norm(Gr_3Dkernal(:,:,3))));%building 3D kernal

       I_pad(m+k,n+k,:)= (1/9)*sum(sum(Gs_3Dkernal.*I_pad(m:m+kp-1,n:n+kp-1,:).*Gr_3Dkernal)); %convolution
    end
end

output_image=I_pad(k+1:M+k,k+1:N+k,:)*12.3;
end

