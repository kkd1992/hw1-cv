function [ Hmag,Hfreq ] = HoughTransform( I,thres,s )
%UNTITLED12 Summary of this function goes here
%   Detailed explanation goes here

[peaks_image]=FindPeaksImage(I,thres); %finding peak response



[M,N,P]=size(I);    

[X,Y]=meshgrid(1:N,1:M); % creating grid of X and Y values

r=zeros(M,N); %initializing (d) values
m=zeros(M,N);
n=zeros(M,N);


 m= X(peaks_image>0) ;%X indexes at which peak response is 255
 n=Y(peaks_image>0); %Y indexes at which peak response is 255
%  figure
% s=20; % angle precision(how many theta to consider
Hmag=zeros(M*N,180/s);
Hfreq=zeros(M*N,180/s);
 i=0;
 figure;
for theta= 0:s*0.0174533:pi %for each theta
    i=i+1;
    clear temp1 temp2;
            r=round(m.*cos(theta)+n.*sin(theta)); %finding d values
%            [~,indexes,~]=(intersect(R,r));  
           [temp1(:,1),temp2(:,1)]=hist(r,unique(r)); % finding intersections temp1 
%            Hough(indexes)=Hough(indexes)+1; 
 Hfreq(1:size(temp1,1),i)=temp1; %contains number of intersections
 Hmag(1:size(temp2,1),i)=temp2; %contains the d value of intersection (rows,colums) indicate (d,theta)
end


  
% Hough_values(Hough>0)=R(Hough>0);



end

