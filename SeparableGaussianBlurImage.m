function [ output_image ] = SeparableGaussianBlurImage( I, sigma )
%The function blurs the image using seperable guassian filter
%   I=input Image
%   sigma = standard deviation

k=1; 
kp=2*k+1;  % size of filter
X=[-1 0 1];
 Gx=(1/2*pi*sigma^2)*exp(-(X.^2)/(2*sigma^2));% creating guassian filter X direction
Gx=Gx./norm(Gx);
 Gy=Gx';

 %building 3D kernals
 Gx_3Dkernal=zeros(1,kp,3);
 Gy_3Dkernal=zeros(kp,1,3);
 for i=1:3
 Gx_3Dkernal(:,:,i)=Gx;
 Gy_3Dkernal(:,:,i)=Gy;
 end

%padding Image
[M,N,P]=size(I);
I_pad=zeros(M+2*k,N+2*k,P);
Ix_pad=zeros(M+2*k,N+2*k,P);
Iy_pad=zeros(M+2*k,N+2*k,P);
I_pad(k+1:M+k,k+1:N+k,:)=I(1:M,1:N,:);

[Mp,Np,P]=size(I_pad);

for m = 1:Mp-kp+1
    for n= 1:Np-kp+1
        
       Ix_pad(m+k,n+k,:)= (1/3)*sum(Gx_3Dkernal.*I_pad(m,n:n+kp-1,:)); %convolution with x kernal
     
    end

end

for m = 1:Mp-kp+1
    for n= 1:Np-kp+1      
               Iy_pad(m+k,n+k,:)= sum(Gy_3Dkernal.*Ix_pad(m:m+kp-1,n,:));%convolution of previous result with y kernal
    end
end

output_image=(Iy_pad(k+1:M+k,k+1:N+k,:));


end

