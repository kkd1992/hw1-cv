function [ I_s ] = BilinearInterpolation( I ,x,y)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
%indexing starts from 1 so x>=1 y>=1; 
%edge values cannot be interpolated so m,n<size(I)
[M,N,P]=size(I);
if x<M
m=floor(x); %finding the corner points
else
    m=M-1;
end
a=x-m;

if y<N
n=floor(y);
else 
    n=N-1;
end
b=y-n;

        I_s=(1-a)*(1-b)*I(m,n)+a*(1-b)*I(m+1,n)+(1-a)*(b)*I(m,n+1)+a*b*I(m+1,n+1);



end

