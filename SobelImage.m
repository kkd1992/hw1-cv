function [ S_grad,S_dir ] = SobelImage( I )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
[M,N,P]=size(I);
if P==3
I_gr=(I(:,:,1)+I(:,:,2)+I(:,:,3))/3;

else 
    I_gr=I;
end

% figure;imshow(I_gr);
Sobel_kx=[-1 0 1;-2 0 2;-1 0 1]; % building sobel kernal
Sobel_ky=Sobel_kx';

I_pad=zeros(M+2,N+2,P);
I_pad(2:M+1,2:N+1)=I_gr(1:M,1:N);

[Mp,Np,P]=size(I_pad);
S_grad_x=zeros(M,N);
S_grad_y=zeros(M,N);

for m = 1:Mp-4
    for n= 1:Np-4
        
       S_grad_x(m+1,n+1)= sum(sum(Sobel_kx.*I_pad(m:m+3-1,n:n+3-1))); %applying filter in x
       S_grad_y(m+1,n+1)= sum(sum(Sobel_kx.*I_pad(m:m+3-1,n:n+3-1)));%applying filter in y
        
    end

end
S_grad=sqrt(S_grad_x.^2+S_grad_y.^2); %finding gradient
S_dir=atan(S_grad_y./S_grad_x); %finding magnitude

    end

